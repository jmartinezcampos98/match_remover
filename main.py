import sys
from subprocess import Popen, PIPE
from typing import Tuple, Union


def exec_popen(bash_statement: str, take_error: bool = False) -> Union[Tuple[bytes, bytes], str]:
    proc = Popen(bash_statement,
                 shell=True,
                 stdout=PIPE,
                 stderr=PIPE,
                 bufsize=-1)
    stdout, stderr = proc.communicate()
    if take_error:
        return stdout, stderr
    else:
        return str(stdout)


def remove_matches(file_path: str, match: str, trim_spaces_search: bool = True) -> None:
    print("Inspeccionando fichero '" + str(file_path) + "' para eliminar las líneas que empiecen por '" + match + "'")
    inspected_file = open(file_path, "r+")
    all_lines = inspected_file.readlines()
    lines_to_write = []
    for file_line in all_lines:
        if not has_match(file_line, match, trim_spaces_search):
            lines_to_write.append(file_line)
    inspected_file.seek(0)
    inspected_file.truncate()
    inspected_file.writelines(lines_to_write)
    inspected_file.close()


def remove_blocks(file_path: str, block_start: str, block_end: str, trim_spaces_search: bool = True) -> None:
    print("Inspeccionando fichero '" + str(file_path) + "'")
    inspected_file = open(file_path, "r+")
    all_lines = inspected_file.readlines()
    same_token_mode = block_start == block_end
    lines_to_write = []
    removing = False
    for file_line in all_lines:
        if not same_token_mode:
            if has_match(file_line, block_start, trim_spaces_search):
                removing = True
            if not removing:
                lines_to_write.append(file_line)
            if has_match_anywhere(file_line, block_end):
                removing = False
        else:
            if has_match_anywhere(file_line, block_start):
                removing = not removing
            elif not removing:
                lines_to_write.append(file_line)
    inspected_file.seek(0)
    inspected_file.truncate()
    inspected_file.writelines(lines_to_write)
    inspected_file.close()


def has_match(line: str, match: str, trim_spaces_search: bool = True) -> bool:
    if trim_spaces_search:
        line = line.lstrip()
    return line.startswith(match)


def has_match_anywhere(line: str, match: str) -> bool:
    return match in line


def remove_matches_on_files_with_extension(complete_path: str, file_extension: str,
                                           single_match: str, match_start: str, match_end: str) -> None:
    name_ending = '.' + file_extension
    extension_length = len(name_ending)
    list_of_files = list(filter(
        lambda x: x[-extension_length:] == name_ending,
        exec_popen("find " + complete_path)[2:-1].split("\\n")
    ))
    for file_path in list_of_files:
        remove_matches(file_path, single_match)
        remove_blocks(file_path, match_start, match_end)


def main() -> None:
    complete_path = sys.argv[1] if len(sys.argv) > 1 else ""
    if complete_path == "":
        exit("Error: se debe especificar la ruta con el primer argumento")
    remove_matches_on_files_with_extension(complete_path, 'php', '//', '/*', '*/')
    remove_matches_on_files_with_extension(complete_path, 'py', '#', '"""', '"""')


if __name__ == '__main__':
    main()
